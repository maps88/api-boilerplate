package action

import (
	"fmt"

	"bitbucket.org/maps88/nucleus/config"
	"bitbucket.org/maps88/nucleus/migration"
	"github.com/urfave/cli"
)

// DBName for application
var DBName = "exhanges"

// Migration command
func Migration(c *cli.Context) error {
	var status bool
	if c.Bool("up") {
		status = true
	}
	if c.Bool("down") {
		status = false
	}

	version, _, err := migration.Migrate("resources/seeds/", DBName, status)
	fmt.Printf("DB Migration[%v] Completed \n", version)

	return err
}

// BeforeMigration command
func BeforeMigration(c *cli.Context) error {
	return config.New()
}
