package action

import (
	"bitbucket.org/maps88/api-boilerplate/internal/api"
	"bitbucket.org/maps88/api-boilerplate/internal/app"
	"bitbucket.org/maps88/api-boilerplate/internal/daos"
	"bitbucket.org/maps88/api-boilerplate/internal/services"
	"bitbucket.org/maps88/nucleus/config"
	"bitbucket.org/maps88/nucleus/exception"
	"bitbucket.org/maps88/nucleus/transport/httplib"

	"github.com/labstack/echo/middleware"
	"github.com/urfave/cli"
)

const (
	configName = "app"
	configPath = "./"
)

// ServeHTTP command
func ServeHTTP(c *cli.Context) error {
	port := c.String("port")
	if port == "" {
		port = config.GetString("port")
	}

	// instantiate new httplib package
	instance := httplib.New(":" + port)

	// set all the required middlewares
	instance.Use(
		middleware.RequestID(),
		middleware.Logger(),
		app.SetRequestScope(false),
	)

	// add more api resources here.
	if err := instance.Set(
		api.NewExchangeRateResource,
		api.NewExchangeTypeResource,
	); err != nil {
		return err
	}

	// override http error handler with exception package
	instance.Echo.HTTPErrorHandler = exception.ErrorHandler

	// initiate healthcheck URL
	instance.HealthCheck(true)

	// serve the http server
	if err := instance.Serve(); err != nil {
		return err
	}

	return nil
}

// BeforeServeHTTP command
func BeforeServeHTTP(c *cli.Context) error {
	// set exception messages
	exception.LoadMessages()

	// initialize services
	services.NewExchangeRateService(daos.NewExchangeRateDAO())
	services.NewExchangeTypeService(daos.NewExchangeTypeDAO())

	return config.New()
}
