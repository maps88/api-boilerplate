package cmd

import (
	"bitbucket.org/maps88/api-boilerplate/cmd/action"
	"github.com/urfave/cli"
)

// ServeHTTP command
var ServeHTTP = cli.Command{
	Name:    "ServeHTTP",
	Aliases: []string{"s"},
	Usage:   "serve HTTP service",
	Flags: []cli.Flag{
		cli.StringFlag{Name: "port", Value: "3000", Usage: "overide default port. default: 3000"},
	},
	Action: action.ServeHTTP,
	Before: action.BeforeServeHTTP,
}

// Migration command
var Migration = cli.Command{
	Name:  "Migration",
	Usage: "DB Migration tools.",
	Flags: []cli.Flag{
		cli.BoolFlag{Name: "up", Usage: "Migrate DB from seeds"},
		cli.BoolFlag{Name: "down", Usage: "Delete DB"},
	},
	Action: action.Migration,
	Before: action.BeforeMigration,
}
