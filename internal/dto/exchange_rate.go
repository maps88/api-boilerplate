package dto

import (
	validation "github.com/go-ozzo/ozzo-validation"
)

// PostExchangeRateRequest struct
type PostExchangeRateRequest struct {
	Rate   float64 `json:"rate" form:"rate"`
	Date   string  `json:"date" form:"date"`
	TypeID int     `json:"exchange_type_id" form:"exchange_type_id"`
}

// Validate validates structs fields.
func (m PostExchangeRateRequest) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.Date, validation.Date("2006-01-02")),
		validation.Field(&m.TypeID, validation.Required),
		validation.Field(&m.Rate, validation.Required),
	)
}

// GetByAverageRequest struct
type GetByAverageRequest struct {
	Date string `json:"date" form:"date"`
}

// GetByAverageResponse struct
type GetByAverageResponse struct {
	Average *float64
	Rate    *float64
}

// Validate validates structs fields.
func (m GetByAverageRequest) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.Date, validation.Required, validation.Date("2006-01-02")),
	)
}

// GetByMinimumRequest struct
type GetByMinimumRequest struct {
	Variance float64 `json:"variance" form:"variance"`
	TypeID   int64   `json:"exchange_type_id" form:"exchange_type_id"`
}

// Validate validates structs fields.
func (m GetByMinimumRequest) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.Variance, validation.Required),
		validation.Field(&m.TypeID, validation.Required),
	)
}
