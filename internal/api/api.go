package api

import (
	"bitbucket.org/maps88/nucleus/transport/httplib"
	"bitbucket.org/maps88/nucleus/utils"
	"github.com/spf13/cast"
)

var (
	// DefaultPageSize const
	DefaultPageSize = 20
	// MaxPageSize const
	MaxPageSize = 1000
)

func getPaginatedList(c *httplib.Context, count int) *utils.PaginatedList {
	page := parseInt(c.QueryParam("page"), 1)
	perPage := parseInt(c.QueryParam("per_page"), DefaultPageSize)
	if perPage <= 0 {
		perPage = DefaultPageSize
	}
	if perPage > MaxPageSize {
		perPage = MaxPageSize
	}
	return utils.NewPaginatedList(page, perPage, count)
}

func parseInt(value string, defaultValue int) int {
	if value == "" {
		return defaultValue
	}
	if result, err := cast.ToIntE(value); err == nil {
		return result
	}
	return defaultValue
}
