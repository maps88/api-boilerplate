package api

import (
	"bitbucket.org/maps88/api-boilerplate/internal/app"
	"bitbucket.org/maps88/api-boilerplate/internal/dto"
	"bitbucket.org/maps88/api-boilerplate/internal/services"
	"bitbucket.org/maps88/nucleus/transport/httplib"
	"github.com/spf13/cast"
)

type exchangeRateResource struct {
	service *services.ExchangeRateService
}

// NewExchangeRateResource configuration
func NewExchangeRateResource(conf *httplib.Config) error {
	resources := &exchangeRateResource{
		service: services.GetExchangeRateService(),
	}

	c := conf.Group("/data")
	c.GET("/average", httplib.NewHandler(resources.getByAverage))
	c.GET("/minimum/:type_id", httplib.NewHandler(resources.getByMinimum))
	c.POST("", httplib.NewHandler(resources.post))
	return nil
}

func (m *exchangeRateResource) post(c *httplib.Context) error {
	ctx := app.GetContext(c)

	var request dto.PostExchangeRateRequest

	if err := c.Bind(&request); err != nil {
		return err
	}
	data, err := m.service.Create(ctx, &request)
	if err != nil {
		return err
	}
	return c.JSONR(data)
}

func (m *exchangeRateResource) getByAverage(c *httplib.Context) error {
	ctx := app.GetContext(c)

	request := dto.GetByAverageRequest{Date: c.QueryParam("date")}
	data, err := m.service.GetAverageLastWeek(ctx, &request)
	if err != nil {
		return err
	}
	return c.JSONR(data)
}

func (m *exchangeRateResource) getByMinimum(c *httplib.Context) error {
	ctx := app.GetContext(c)

	request := dto.GetByMinimumRequest{
		Variance: cast.ToFloat64(c.QueryParam("variance")),
		TypeID:   cast.ToInt64(c.Param("type_id")),
	}

	data, err := m.service.GetMaxMinimum(ctx, &request)
	if err != nil {
		return err
	}
	return c.JSONR(data)
}
