package api

import (
	"bitbucket.org/maps88/api-boilerplate/internal/app"
	"bitbucket.org/maps88/api-boilerplate/internal/models"
	"bitbucket.org/maps88/api-boilerplate/internal/services"
	"bitbucket.org/maps88/nucleus/transport/httplib"
	"github.com/spf13/cast"
)

type exchangeTypeResource struct {
	service *services.ExchangeTypeService
}

// NewExchangeTypeResource configuration
func NewExchangeTypeResource(conf *httplib.Config) error {
	resources := &exchangeTypeResource{
		service: services.GetExchangeTypeService(),
	}

	c := conf.Group("/types")
	c.GET("/:id", httplib.NewHandler(resources.get))
	c.GET("", httplib.NewHandler(resources.query))
	c.PUT("", httplib.NewHandler(resources.put))
	c.DELETE("", httplib.NewHandler(resources.delete))
	c.POST("", httplib.NewHandler(resources.post))

	return nil
}

func (e *exchangeTypeResource) get(c *httplib.Context) error {
	ctx := app.GetContext(c)
	data, err := e.service.Get(ctx, cast.ToInt64(c.Param("id")))
	if err != nil {
		return err
	}
	return c.JSONR(data)
}

func (e *exchangeTypeResource) put(c *httplib.Context) error {
	var model models.ExchangeType

	if err := c.Bind(&model); err != nil {
		return err
	}

	ctx := app.GetContext(c)
	data, err := e.service.Update(ctx, cast.ToInt64(c.Param("id")), &model)
	if err != nil {
		return err
	}

	return c.JSONR(data)

}

func (e *exchangeTypeResource) delete(c *httplib.Context) error {
	ctx := app.GetContext(c)

	err := e.service.Delete(ctx, cast.ToInt64(c.Param("id")))
	if err != nil {
		return err
	}

	return c.JSONR(map[string]string{"status": "aknowledged"})
}

func (e *exchangeTypeResource) post(c *httplib.Context) error {
	var model models.ExchangeType

	if err := c.Bind(&model); err != nil {
		return err
	}

	ctx := app.GetContext(c)
	data, err := e.service.Create(ctx, &model)
	if err != nil {
		return err
	}

	return c.JSONR(data)

}

func (e *exchangeTypeResource) query(c *httplib.Context) error {
	ctx := app.GetContext(c)
	count, err := e.service.Count(ctx)
	if err != nil {
		return err
	}

	paginatedList := getPaginatedList(c, cast.ToInt(count))
	items, err := e.service.Query(ctx, "", paginatedList.Offset(), paginatedList.Limit())
	if err != nil {
		return err
	}

	paginatedList.Items = items
	return c.JSONR(paginatedList)
}
