package models

import "time"

// ExchangeRate reflect to table structure
type ExchangeRate struct {
	ID           int
	ExchangeType ExchangeType
	Average      *float64   `json:"Average,omitempty"`
	Rate         float64    `db:"rate"`
	CreatedAt    *time.Time `db:"created_at"`
}
