package models

import (
	validation "github.com/go-ozzo/ozzo-validation"
)

// ExchangeType reflect to table structure
type ExchangeType struct {
	ID   int    `db:"id"`
	From string `db:"from"`
	To   string `db:"to"`
}

// Validate validates the Artist fields.
func (m ExchangeType) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.From, validation.Required),
		validation.Field(&m.To, validation.Required),
	)
}
