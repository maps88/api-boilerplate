package daos

import (
	"context"
	"database/sql"
	"fmt"

	"bitbucket.org/maps88/api-boilerplate/internal/app"
	"bitbucket.org/maps88/api-boilerplate/internal/models"
	"github.com/jmoiron/sqlx"
)

// IExchangeRateDAO interface
type IExchangeRateDAO interface {
	// GetAverageLastWeek returns the entities with average 7 days.
	GetAverageLastWeek(ctx context.Context, date string) (model []models.ExchangeRate, err error)
	// GetMaxMinimum
	GetMaxMinimum(ctx context.Context, variance float64, typeID int64) (model []models.ExchangeRate, err error)
	// Create saves a new entity in the storage.
	Create(ctx context.Context, model *models.ExchangeRate) (lastInsertedID int64, err error)
	// Get
	Get(ctx context.Context, id int) (*models.ExchangeRate, error)
}

type exchangeRateDAO struct{}

// NewExchangeRateDAO creates IExchangeRate
func NewExchangeRateDAO() IExchangeRateDAO {
	return new(exchangeRateDAO)
}

// Get returns the entity with the specified ID.
func (e *exchangeRateDAO) GetAverageLastWeek(ctx context.Context, date string) (results []models.ExchangeRate, err error) {
	rs := app.GetRequestScope(ctx)

	rows, err := rs.SQL("slave").
		Queryx(fmt.Sprintf(`
		SELECT
			et.from, et.to, et.id, t1.created_at, t1.rate,
			IF ( COUNT(1) >= 7, AVG(t2.rate),NULL) as avg
		FROM exchange_rates t1, exchange_rates t2
			INNER JOIN exchange_types et ON et.id = t2.exchange_type_id
		WHERE t2.created_at between DATE_SUB(date(t1.created_at),INTERVAL 6 day) AND t1.created_at
			AND t1.created_at LIKE '%v%%'
		GROUP BY et.from , et.to
		ORDER BY created_at desc, et.id
	`, date))

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var model models.ExchangeRate
		if err := rows.Scan(
			&model.ExchangeType.From,
			&model.ExchangeType.To,
			&model.ExchangeType.ID,
			&model.CreatedAt,
			&model.Rate,
			&model.Average,
		); err != nil {
			return nil, err
		}

		results = append(results, model)
	}

	return results, nil
}

// GetMinimum returns the entity with the specified ID.
func (e *exchangeRateDAO) GetMaxMinimum(ctx context.Context, variance float64, typeID int64) (results []models.ExchangeRate, err error) {
	rs := app.GetRequestScope(ctx)

	rows, err := rs.SQL("slave").
		Queryx(`
		SELECT er.id, er.created_at, er.rate, et.id, et.from, et.to
		FROM exchange_rates er
		INNER JOIN exchange_types et ON et.id = er.exchange_type_id
		WHERE er.exchange_type_id = ? AND er.rate >= ? 
		ORDER BY er.created_at DESC
		LIMIT 7
	`, typeID, variance)

	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var model models.ExchangeRate
		if err := rows.Scan(
			&model.ID,
			&model.CreatedAt,
			&model.Rate,
			&model.ExchangeType.ID,
			&model.ExchangeType.From,
			&model.ExchangeType.To,
		); err != nil {
			return nil, err
		}

		results = append(results, model)
	}

	return results, nil
}

func (e *exchangeRateDAO) Get(ctx context.Context, id int) (*models.ExchangeRate, error) {
	var exchangeRate models.ExchangeRate
	rs := app.GetRequestScope(ctx)

	err := rs.SQL("slave").
		QueryRowx(`
			SELECT a.id, b.from, b.to, b.id, a.created_at, a.rate
			FROM exchange_rates a
			INNER JOIN exchange_types b ON b.id = a.exchange_type_id
			WHERE a.id = ?
		`, id).Scan(
		&exchangeRate.ID,
		&exchangeRate.ExchangeType.From,
		&exchangeRate.ExchangeType.To,
		&exchangeRate.ExchangeType.ID,
		&exchangeRate.CreatedAt,
		&exchangeRate.Rate,
	)

	return &exchangeRate, err
}

// Create saves a new entity in the storage.
func (e *exchangeRateDAO) Create(ctx context.Context, model *models.ExchangeRate) (lastInsertedID int64, err error) {
	model.ID = 0
	var result sql.Result

	if err := Transact(ctx, func(tx *sqlx.Tx) (err error) {
		result, err = tx.Exec(`
			INSERT INTO exchange_rates
			(rate, exchange_type_id, created_at)
			VALUES
			(?, ?, ?)
		`, model.Rate, model.ExchangeType.ID, model.CreatedAt)

		return
	}); err != nil {
		return 0, err
	}

	return result.LastInsertId()
}
