package daos

import (
	"context"
	"database/sql"

	"bitbucket.org/maps88/api-boilerplate/internal/app"
	"bitbucket.org/maps88/api-boilerplate/internal/models"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

// IExchangeTypeDAO interface
type IExchangeTypeDAO interface {
	// Get returns the entity with the specified ID.
	Get(ctx context.Context, id int64) (model *models.ExchangeType, err error)
	// Count returns the number of entities.
	Count(ctx context.Context) (count int64, err error)
	// Create saves a new entity in the storage.
	Create(ctx context.Context, request *models.ExchangeType) (lastInsertedID int64, err error)
	// Update updates entity with given ID in the storage.
	Update(ctx context.Context, id int64, request *models.ExchangeType) error
	// Delete removes entity with given ID from the storage.
	Delete(ctx context.Context, id int64) error
	// Query returns the list of entity with the given query, offset and limit.
	Query(ctx context.Context, query string, offset, limit uint64) (models []models.ExchangeType, err error)
}

type exchangeTypeDAO struct{}

// NewExchangeTypeDAO creates IExchangeRate
func NewExchangeTypeDAO() IExchangeTypeDAO {
	return new(exchangeTypeDAO)
}

// Get returns the entity with the specified ID.
func (e exchangeTypeDAO) Get(ctx context.Context, id int64) (result *models.ExchangeType, err error) {
	var model models.ExchangeType
	rs := app.GetRequestScope(ctx)

	err = rs.SQL("slave").
		QueryRowx(`SELECT * FROM exchange_types WHERE id = ?`, id).
		StructScan(&model)
	if err != nil {
		return nil, err
	}

	return &model, nil
}

// Count returns the number of entities.
func (e exchangeTypeDAO) Count(ctx context.Context) (count int64, err error) {
	rs := app.GetRequestScope(ctx)

	err = rs.SQL("slave").QueryRowx("SELECT COUNT(*) FROM exchange_types").Scan(&count)
	return
}

// Create saves a new entity in the storage.
func (e exchangeTypeDAO) Create(ctx context.Context, request *models.ExchangeType) (lastInsertedID int64, err error) {
	request.ID = 0
	var result sql.Result

	if err := Transact(ctx, func(tx *sqlx.Tx) (err error) {
		result, err = tx.NamedExec(`
			INSERT INTO exchange_types
			(from, to)
			VALUES
			(:from, :to)
		`, request)

		return
	}); err != nil {
		return 0, err
	}

	return result.LastInsertId()
}

// Update updates entity with given ID in the storage.
func (e exchangeTypeDAO) Update(ctx context.Context, id int64, request *models.ExchangeType) error {
	request.ID = int(id)

	return Transact(ctx, func(tx *sqlx.Tx) (err error) {
		if _, err := tx.NamedExec(`
			UPDATE exchange_types
			SET
				from = COALESCE(:from, name),
				to = COALESCE(:to, phone),
			WHERE id = :id
		`, request); err != nil {
			return err
		}
		return
	})
}

// Delete removes entity with given ID from the storage.
func (e exchangeTypeDAO) Delete(ctx context.Context, id int64) error {
	return Transact(ctx, func(tx *sqlx.Tx) (err error) {
		if _, err := tx.Exec(`DELETE FROM exchange_types WHERE id = ?
		`, id); err != nil {
			return err
		}
		return
	})
}

// Query returns the list of entity with the given query, offset and limit.
func (e exchangeTypeDAO) Query(ctx context.Context, query string, offset, limit uint64) (results []models.ExchangeType, err error) {
	rs := app.GetRequestScope(ctx)

	active := sq.Select("*").From("exchange_types")
	if query != "" {
		active = active.Where(query)
	}

	active = active.Offset(offset).Limit(limit)
	sql, args, err := active.ToSql()
	if err != nil {
		return results, err
	}

	rows, err := rs.SQL("slave").Queryx(sql, args...)
	if err != nil {
		return results, err
	}
	defer rows.Close()

	for rows.Next() {
		var model models.ExchangeType
		if err := rows.StructScan(&model); err != nil {
			return results, err
		}

		results = append(results, model)
	}
	return
}
