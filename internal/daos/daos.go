package daos

import (
	"context"

	"bitbucket.org/maps88/api-boilerplate/internal/app"
	"github.com/jmoiron/sqlx"
)

// TxFunc type function
type TxFunc func(*sqlx.Tx) error

// Transact defer rollback and commit
func Transact(ctx context.Context, txFunc TxFunc) (err error) {
	rs := app.GetRequestScope(ctx)

	tx, err := rs.SQL("master").BeginTxx(ctx, nil)
	if err != nil {
		return err
	}

	defer func() {
		if p := recover(); p != nil {
			tx.Rollback()
			panic(p) // re-throw panic after Rollback
		} else if err != nil {
			tx.Rollback()
		} else {
			err = tx.Commit()
		}
	}()
	err = txFunc(tx)
	return err
}
