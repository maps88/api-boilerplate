package daos

import (
	"context"
	"testing"
	"time"

	"bitbucket.org/maps88/api-boilerplate/internal/app"
	"github.com/stretchr/testify/assert"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func requestScope() context.Context {
	rs := app.NewRequestScope(true)
	return context.WithValue(context.Background(), app.KeyBearer, rs)
}

func TestExchangeRate_GetAverageLastWeek(t *testing.T) {
	ctx := requestScope()
	rs := app.GetRequestScope(ctx)

	input := "2012-10-30"

	rows := sqlmock.NewRows([]string{"from", "to", "exchange_type_id", "created_at", "rate", "avg"})
	rows.AddRow("USD", "JYP", 1, time.Now(), 0.321312, 0.2131231)

	t.Run("t1: Get existing record", func(tt *testing.T) {
		rs.MockDB().ExpectQuery("SELECT").WillReturnRows(rows)
		if res, err := NewExchangeRateDAO().GetAverageLastWeek(ctx, input); assert.NoError(tt, err) {
			assert.Equal(tt, res[0].ExchangeType.ID, 1)
			assert.Equal(tt, res[0].ExchangeType.From, "USD")
			assert.Equal(tt, res[0].ExchangeType.To, "JYP")
		}
	})
}
