package services

import (
	"context"
	"time"

	"bitbucket.org/maps88/api-boilerplate/internal/dto"
	"bitbucket.org/maps88/api-boilerplate/internal/models"

	"bitbucket.org/maps88/api-boilerplate/internal/daos"
)

var exchangeRateService *ExchangeRateService

// ExchangeRateService struct
type ExchangeRateService struct {
	dao daos.IExchangeRateDAO
}

// NewExchangeRateService creates a new services with the given DAO
func NewExchangeRateService(dao daos.IExchangeRateDAO) *ExchangeRateService {
	if exchangeRateService == nil {
		exchangeRateService = &ExchangeRateService{dao: dao}
	}

	return exchangeRateService
}

// GetExchangeRateService get the initialized service
func GetExchangeRateService() *ExchangeRateService {
	return exchangeRateService
}

// GetMaxMinimum services
func (m *ExchangeRateService) GetMaxMinimum(ctx context.Context, request *dto.GetByMinimumRequest) ([]models.ExchangeRate, error) {
	if err := request.Validate(); err != nil {
		return nil, err
	}
	return m.dao.GetMaxMinimum(ctx, request.Variance, request.TypeID)
}

// GetAverageLastWeek services
func (m *ExchangeRateService) GetAverageLastWeek(ctx context.Context, request *dto.GetByAverageRequest) ([]models.ExchangeRate, error) {
	if err := request.Validate(); err != nil {
		return nil, err
	}
	return m.dao.GetAverageLastWeek(ctx, request.Date)
}

// Create services
func (m *ExchangeRateService) Create(ctx context.Context, request *dto.PostExchangeRateRequest) (*models.ExchangeRate, error) {
	var model models.ExchangeRate
	if err := request.Validate(); err != nil {
		return nil, err
	}

	// err will never be trigger, due to validation.
	t, _ := time.Parse("2006-01-02", request.Date)

	// allow empty date request
	if request.Date == "" {
		t = time.Now()
	}

	model.CreatedAt = &t
	model.ExchangeType.ID = request.TypeID
	model.Rate = request.Rate

	id, err := m.dao.Create(ctx, &model)
	if err != nil {
		return nil, err
	}

	return m.dao.Get(ctx, int(id))
}
