package services

import (
	"context"

	"bitbucket.org/maps88/api-boilerplate/internal/daos"
	"bitbucket.org/maps88/api-boilerplate/internal/models"
)

var exchangeTypeService *ExchangeTypeService

// ExchangeTypeService service
type ExchangeTypeService struct {
	dao daos.IExchangeTypeDAO
}

// NewExchangeTypeService creates a new services with the given DAO
func NewExchangeTypeService(dao daos.IExchangeTypeDAO) *ExchangeTypeService {
	if exchangeTypeService == nil {
		exchangeTypeService = &ExchangeTypeService{dao: dao}
	}

	return exchangeTypeService
}

// GetExchangeTypeService get the initialized service
func GetExchangeTypeService() *ExchangeTypeService {
	return exchangeTypeService
}

// Create services
func (e *ExchangeTypeService) Create(ctx context.Context, request *models.ExchangeType) (result *models.ExchangeType, err error) {
	if err := request.Validate(); err != nil {
		return nil, err
	}
	id, err := e.dao.Create(ctx, request)
	if err != nil {
		return nil, err
	}
	return e.dao.Get(ctx, id)
}

// Update services
func (e *ExchangeTypeService) Update(ctx context.Context, id int64, request *models.ExchangeType) (result *models.ExchangeType, err error) {
	if err := request.Validate(); err != nil {
		return nil, err
	}

	if err := e.dao.Update(ctx, id, request); err != nil {
		return nil, err
	}

	return e.dao.Get(ctx, id)
}

// Delete services
func (e *ExchangeTypeService) Delete(ctx context.Context, id int64) (err error) {
	return e.dao.Delete(ctx, id)
}

// Get services
func (e *ExchangeTypeService) Get(ctx context.Context, id int64) (result *models.ExchangeType, err error) {
	return e.dao.Get(ctx, id)
}

// Count services
func (e *ExchangeTypeService) Count(ctx context.Context) (result int64, err error) {
	return e.dao.Count(ctx)
}

// Query services
func (e *ExchangeTypeService) Query(ctx context.Context, query string, offset, limit int) (result []models.ExchangeType, err error) {
	return e.dao.Query(ctx, query, uint64(offset), uint64(limit))
}
