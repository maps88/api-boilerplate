package app

import (
	"context"

	"bitbucket.org/maps88/nucleus/storage/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

type key string

// KeyBearer custom handler
var KeyBearer key = "RequestScope"

// RequestScope contains the application-specific information that are carried around in a request.
type RequestScope interface {
	MockDB() sqlmock.Sqlmock
	// SQL returns the currently active database transaction that can be used for DB query purpose
	SQL(connType string) *sqlx.DB
}

type requestScope struct {
	sql *mysql.DB // the currently active transaction
}

func (r *requestScope) SQL(connType string) *sqlx.DB {
	return r.sql.MustConnect(connType)
}

func (r *requestScope) MockDB() sqlmock.Sqlmock {
	return r.sql.Mock
}

// NewRequestScope creates a new RequestScope with the current request information.
func NewRequestScope(isMock bool) RequestScope {
	return &requestScope{
		sql: mysql.New(isMock),
	}
}

// GetRequestScope returns the RequestScope of the current request.
func GetRequestScope(ctx context.Context) RequestScope {
	return ctx.Value(KeyBearer).(RequestScope)
}

// SetRequestScope is an echo middleware that injects sql instances into echo context.
func SetRequestScope(isMock bool) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			rs := NewRequestScope(isMock)

			ctx := context.WithValue(c.Request().Context(), KeyBearer, rs)
			c.SetRequest(c.Request().WithContext(ctx))

			return next(c)
		}
	}
}
