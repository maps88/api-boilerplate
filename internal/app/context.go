package app

import (
	"context"

	"github.com/labstack/echo"
)

func GetContext(c echo.Context) context.Context {
	return c.Request().Context()
}
