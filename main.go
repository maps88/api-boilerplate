package main

import (
	"fmt"
	"os"

	"bitbucket.org/maps88/api-boilerplate/cmd"
	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "API Boilerplate"
	app.Usage = "internalAPI for API Boilerplate."
	app.Commands = []cli.Command{
		cmd.ServeHTTP,
		cmd.Migration,
	}

	if err := app.Run(os.Args); err != nil {
		fmt.Println(err)
		os.Exit(0)
	}
}
