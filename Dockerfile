FROM golang:1.10-alpine

ARG git_tag
ARG git_commit

RUN apk add --no-cache git build-base curl
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

WORKDIR /go/src/bitbucket.org/maps88/api-boilerplate

ADD ./Gopkg.lock ./Gopkg.lock
ADD ./Gopkg.toml ./Gopkg.toml
RUN dep ensure -vendor-only

ADD . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "-s -X bitbucket.org/maps88/api-boilerplate/cmd.Version=$git_tag -X bitbucket.org/maps88/api-boilerplate/cmd.BuildTime=`TZ=UTC date -u '+%Y-%m-%dT%H:%M:%SZ'` -X bitbucket.org/maps88/api-boilerplate/cmd.GitHash=$git_commit" -a -installsuffix cgo -o api-boilerplate

ENTRYPOINT ["./api-boilerplate"]