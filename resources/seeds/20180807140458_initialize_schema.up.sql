CREATE TABLE IF NOT EXISTS `exchange_types` (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `from` varchar(10) DEFAULT NULL,
    `to` varchar(10) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `idx_from_to` (`from`, `to`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO exchange_types (`id`, `from`, `to`) VALUES
(1, "GBP", "USD"),
(2, "USD", "GBP"),
(3, "USD", "IDR"),
(4, "JPY", "IDR");

CREATE TABLE IF NOT EXISTS `exchange_rates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `exchange_type_id` int(11) unsigned NOT NULL,
  `rate` decimal(19,6) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exchange_type_id` (`exchange_type_id`),
  KEY `idx_exchange_rates_created_at` (`created_at`),
  CONSTRAINT `exchange_rates_ibfk_1` FOREIGN KEY (`exchange_type_id`) REFERENCES `exchange_types` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
