# API Boilerplate [![CircleCI](https://circleci.com/bb/maps88/api-boilerplate.svg?style=svg)](https://circleci.com/bb/maps88/api-boilerplate) [![codecov](https://codecov.io/bb/gellato/merchant/branch/master/graph/badge.svg?token=NWnxzYwaAP)](https://codecov.io/bb/maps88/api-boilerplate)
RESTful API boilerplate built with GO.

# Documentation
- [Wiki](https://bitbucket.org/maps88/api-boilerplate/wiki/Home)
- [API Documentation](https://bitbucket.org/maps88/api-boilerplate/wiki/References).

# Dependencies
- [Go 1.8](https://golang.org/dl/) or later.
- [MySQL server](https://dev.mysql.com/downloads/installer/) : database server
- [nucleus](https://bitbucket.org/maps88/nucleus) : package library.
- [dep](https://github.com/golang/dep) : dependencies management.

# Architecture

## __cmd__
cli actions, you can setup multiple action type like 'serve HTTP' or 'Migration'

## __internal__
Private application and library code. This is the code you don't want others importing in their applications or libraries.

- app
- api
- daos
- dto
- models
- services

## __resources__
stored all assets such as configuration, database sql migration schema.

# Feature
1. CI/CD using circleCI
2. dockerize application
3. package separation, to be easier to write unit test.

# Install (Manual)
1. go get bitbucket.org/maps88/api-boilerplate or clone this repository.
2. dep ensure -v
3. go build
4. (optional) if database not yet been setup run ./api-boilerplate Migration --up
6. run the CLI : ./api-boilerplate ServeHTTP
7. check help : ./api-boilerplate --help

# Install (Docker)
1. docker-compose build
2. docker-compose up

